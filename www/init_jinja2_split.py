import logging; logging.basicConfig(level=logging.INFO)
from datetime import datetime
from aiohttp import web
import os
from jinja2 import Environment, FileSystemLoader
def init_jinja2(app, **kw):
    logging.info('init jinja2...')
    # 定义一个字典,用于修改jinja2默认的Environment
    # 这里实际修改了autoescape,默认为False，修改为True
    options = dict(
        autoescape = kw.get('autoescape', True),
        block_start_string = kw.get('block_start_string', '{%'),
        block_end_string = kw.get('block_end_string', '%}'),
        variable_start_string = kw.get('variable_start_string', '{{'),
        variable_end_string = kw.get('variable_end_string', '}}'),
        auto_reload = kw.get('auto_reload', True)
    )
    print(options)
    # 从传递的字典kw中获取path属性
    # 本次没有传递，使用为空
    path = kw.get('path', None)
    # path是空的所以执行了if下的语句
    # 其中os.path.abspath(__file__)获取当前执行文件的绝对路径  本次为 d:\learn-python3\实战\awesome-python3-webapp\www\init_jinja2_split.py
    # os.path.dirname(os.path.abspath(__file__))通过绝对路径获取到文件夹  本次为 d:\learn-python3\实战\awesome-python3-webapp\www\
    # os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')把文件夹和'templates'拼接 本次为 d:\learn-python3\实战\awesome-python3-webapp\www\templates
    # 即相当于执行以下代码获得了当前执行的py文件的文件夹下的文件夹templates完整路径
    if path is None:
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
    logging.info('set jinja2 template path: %s' % path)  
    # 修改默认的Environment()默认为一个字典存储了各种键值对，以键值对或者字典的方式传递
    # FileSystemLoader为文件系统加载器，不需要模板文件存在某个Python包下，可以直接访问系统中的文件
    # 如果默认的Environment()没有传递的键值对则添加，如果有则修改
    # 例如默认的有key为'autoescape'对应的value为False options字典有这个key但是值为True所以修改了这个属性
    env = Environment(loader=FileSystemLoader(path), **options)
    #print(env.__dict__)
    print(kw)
    # 从传递的字典中去查找属性filters，本次是有传递的不为空对应的值是一个字典{'datetime':datetime_filter}
    filters = kw.get('filters', None)
    # 不为空，所以执行if语句，把这个字典的key,value添加至默认env的属性'filter'中
    # 默认的属性'filter'值为一个字典，字典的key,value为方法名和对应的一个函数
    if filters is not None:
        for name, f in filters.items():
            env.filters[name] = f 
    # 把修改后的env赋值给app的属性'__templating__'
    app['__templating__'] = env


# 时间转换函数，传递一个时间戳计算与当前时间的相差时间，如果相差查过小于60秒则返回1分钟前，以此类推，如果相差查过365天则返回确切的时间戳对应的时间
def datetime_filter(t):
    delta = int(time.time() - t)
    if delta < 60:
        return u'1分钟前'
    if delta < 3600:
        return u'%s分钟前' % (delta // 60)
    if delta < 86400:
        return u'%s小时前' % (delta // 3600)
    if delta < 604800:
        return u'%s天前' % (delta // 86400)
    dt = datetime.fromtimestamp(t)
    return u'%s年%s月%s日' % (dt.year, dt.month, dt.day)


# 通过web生成一个app
app = web.Application()
# 把app和filters=dict(datetime=datetime_filter)作为参数传递给初始化函数init_jinja2
init_jinja2(app, filters=dict(datetime=datetime_filter))

# 为了对比dict1存储了默认的Enviroment().__dict__
# dict1存储了进过初始化修改以后的__dict__
dict1 = Environment().__dict__
dict2 = app['__templating__'].__dict__

# 为了便于对比使用key,value的方法打印
# for k,v in dict1.items():
#     print(k,v)

# 只打印cache信息进行对比
for k,v in dict2.items():
    if k == 'cache':
        print(k,v)

# 自定义字典
r = {'__template__': 'test.html', 'users': [{'id': '001637895583499da19dda790484f7e8938266ecc126496000', 'email': 'test@qq.com', 'passwd': '1234567890', 'admin': 0, 'name': 'Test', 'image': 'about:blank', 'created_at': 1637895583.49905}]}
template = r.get('__template__')
#print(template)
print("#### app['__templating__']")
# 打印jinjia2的env,这里是经过修改的然后发到app的属性'__templating__'中，没有修改默认的是Environment()
print(app['__templating__'])
# <jinja2.environment.Environment object at 0x0000021289BD1408>
print("#### app['__templating__'].get_template(template))")
# get_template获取模板对象，获取的前提是这个变量已经把需要查找模板的路径定义好了，这个代码实现了查找模板的路径 env = Environment(loader=FileSystemLoader(path), **options)
# 其中本次的path为 d:\learn-python3\实战\awesome-python3-webapp\www\templates 如果没有定义模板查找的路径则会因为找不到模板报错
# 返回为模板对象，然后再字典属性'cache'中添加对应的信息
print(app['__templating__'].get_template(template))
# <Template 'test.html'>
dict3 = app['__templating__'].__dict__
# render方法，往模板添加对象，当模板使用时调用它
# 相当于往模板test.html中添加r这个字典，然后再模板中jinja语法去，调用这个字典
# 例如模板test.html中有以下代码 {% for u in users %}则就是遍历users这个变量然后从中取出想要的值例如u.name,u.email
print("app['__templating__'].get_template(template).render(**r)")
print(app['__templating__'].get_template(template).render(**r))
# 二进制编码，页面返回需要编码后返回
# print(app['__templating__'].get_template(template).render(**r).encode('utf-8'))

# 打印获得get_template('test.html')后属性'cache'去和没有使用get_template对比
for k,v in dict3.items():
    if k == 'cache':
        print(k,v)
