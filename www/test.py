import orm
import asyncio
from models import User, Blog, Comment

# 定义协程函数test
async def test(loop):
    # 创建连接池
    await orm.create_pool(loop=loop,user='www-data', password='www-data', db='awesome')
    # 使用类User实例化一个对象，其实就是相当于创建了一个字典
    # id和created_at没有传入，使用默认值next_id和time.time
    # 如果id和created_at在初始化的时候传入会优先获取初始化时传入的值，不使用默认值
    u = User(name='Test', email='test@qq.com', passwd='1234567890', image='about:blank')
    # print(dir(u))
    # print(u.__fields__)
    await u.save()
    # print(u.__fields__)
    ## 网友指出添加到数据库后需要关闭连接池，否则会报错 RuntimeError: Event loop is closed
    orm.__pool.close()
    await orm.__pool.wait_closed()
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(test(loop))
    loop.close()

