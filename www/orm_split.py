############################################
# 拆分解析异步建立数据库连接池 start

import asyncio,logging,aiomysql
# 定义日志输出级别为INFO，默认为WARNING
logging.basicConfig(level=logging.INFO)
# 定义日志输出函数，在其他函数内部调用即可输出info日志
def log(sql,args=()):
    logging.info('SQL:%s' % sql)
     
# 定义创建连接池的协程函数，该协程函数传递参数为一个事件循环对象loop
# 和一个字典，字典内为创建连接池所需要的参数例如连接数据库的host,port,user,password,db等信息
# async def create_pool(loop,**kw):
#     logging.info('create database connection pool...')
#     # 连接池为全局变量，其他函数也可以调用
#     global __pool
#     __pool = await aiomysql.create_pool(
#         # 获取字典kw的host属性，如果没有获取到则返回自定义的localhost
#         # 即如果字典没有传递host为key的值则默认使用localhost
#         host=kw.get('host', 'localhost'),
#         port=kw.get('port', 3306),
#         # 从字典获取user,password,db等信息
#         user=kw['user'],
#         password=kw['password'],
#         db=kw['db'],
#         charset=kw.get('charset', 'utf8'),
#         autocommit=kw.get('autocommit', True),
#         maxsize=kw.get('maxsize', 10),
#         minsize=kw.get('minsize', 1),
#         loop=loop
#     )

# 演示创建连接池
# 创建事件循环对象
# loop = asyncio.get_event_loop()
# # 定义连接池的参数
# kw = {'user':'www-data','password':'www-data','db':'awesome'}
# loop.run_until_complete(create_pool(loop=loop,**kw))
# print(__pool)

# async def select(sql):
#     log(sql)
#     # 通过连接池__pool创建一个 <class 'aiomysql.connection.Connection'>对象conn
#     # 前提是运行过协程函数create_pool已经创建了连接池__pool
#     # 使用这个语句运行会报警告DeprecationWarning,但是可以正常运行
#     with await __pool as conn:   
#     # 使用这个语句不报警告
#     # async with __pool.acquire() as conn:
#         # print(type(conn))
#         # 使用await conn.cursor(aiomysql.DictCursor)创建一个<class 'aiomysql.cursors.DictCursor'>对象赋值给cur
#         cur = await conn.cursor(aiomysql.DictCursor)
#         # print(type(cur))
#         # 执行搜索sql语句为函数传递的sql语句
#         await cur.execute(sql)
#         # 把所有搜索结果返回，返回为一个list 内部元素为搜索结果对应的0至多个字典
#         # 方法cur.fetchmany(size)返回size设置的指定数量的最多返回结果
#         # 即假如返回结果有5条把size设置为3则最多返回3条
#         rs = await cur.fetchall()
#         # 关闭
#         await cur.close()
#         return rs
# res=loop.run_until_complete(select('select * from users'))
# print(res)

# # Insert, Update, Delete start
# async def execute(sql,args):
#     log(sql)
#     with (await __pool) as conn:
#         try:
#             # 使用await conn.cursor()创建<class 'aiomysql.cursors.Cursor'>对象
#             cur = await conn.cursor()
#             # print(type(cur))
#             await cur.execute(sql.replace('?','%s'),args)
#             affected = cur.rowcount
#             await cur.close()
#         except BaseException as e:
#             raise
#         return affected
# Insert, Update, Delete end

# res = loop.run_until_complete(execute('insert into users values (123,"test1@qq.com","123456789",0,"liuym","http://image",1111111111)',[]))
# 拆分解析异步建立数据库连接池 end
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

# ############################################
# # 拆分解析类User的使用metaclass的修改过程 start
# import time
# # 根据传递的整数生成一个字符串?,?,?...用于占位符
# def create_args_string(num):
#     L = []
#     for n in range(num):
#         L.append('?')
#     return ', '.join(L)

# class ModelMetaclass(type):

#     def __new__(cls, name, bases, attrs):
#         # 排除Model类本身,即针对Model不做任何修改原样返回:
#         if name=='Model':
#             return type.__new__(cls, name, bases, attrs)
#         # 获取table名称:
#         # 从字典attrs中获取属性__table__,如果在类中没有定义这个属性则返回None
#         # 如果在属性中没有定义但是我们可以从参数name中获取到就是类名
#         # 例如我们对类User进行重新创建,在类User中已经定义了属性 __table__ = 'users'
#         # 所以我们优先得到的表名就是'users',假如没有定义则就是类名'User'
#         tableName = attrs.get('__table__', None) or name
#         # 输出日志
#         logging.info('found model: %s (table: %s)' % (name, tableName))
#         # 为了便于观察我们打印对类修改前的attrs字典
#         print("修改前attrs:%s" % attrs)
#         # 获取所有的Field和主键名:
#         # 定义一个空字典用于存储需要定义的类中除了默认属性以为定义的属性
#         # 例如本次我们针对类User则使用mappings字典存储'id','email','passwd','admin','name','image','create_at'的属性
#         # 它们对应的属性值是一个实例化以后的实例，例如id对应的属性值是通过类StringField实例化后的实例
#         mappings = dict()
#         # fields表用于存储普通的字段名称，即除了主键以外的其他字段
#         # 例如针对User类则在fields存储字段'email','passwd','admin','name','image','create_at'的名称
#         fields = []
#         # primaryKey用于存储主键字段名
#         # 例如针对类User的主键名为'id'
#         primaryKey = None
#         # 以key,value的方式遍历attrs字典
#         for k, v in attrs.items():
#             # 如果对应的value是Field的子集则把对应的key value追加至字典mappings
#             if isinstance(v, Field):
#                 logging.info('  found mapping: %s ==> %s' % (k, v))
#                 mappings[k] = v
#                 # 然后通过实例的属性primary_key去找主键，如果找到了主键则赋值给primaryKey
#                 # 如果不是主键的字段则追加至fields这个list
#                 # 一个表只能有一个主键，如果有多个主键则抛出RuntimeError错误，错误提示为重复的主键
#                 if v.primary_key:
#                     # 找到主键:
#                     if primaryKey:
#                         raise RuntimeError('Duplicate primary key for field: %s' % k)
#                     primaryKey = k
#                 else:
#                     fields.append(k)
#         # 如果表内没有定义主键则抛出错误主键没有发现
#         if not primaryKey:
#             raise RuntimeError('Primary key not found.')
#         # 已经把对应的key value存储至字典以后，从原attrs中属性中删除对应的key value
#         # 如果不删除则类属性和实例属性会冲突
#         # 例如类User需要从原attrs中删除key值为 'id','email','passwd','admin','name','image','create_at'的元素
#         for k in mappings.keys():
#             attrs.pop(k)
#         # 把存储除主键之外的字典list元素加一个``
#         # 例如原fields为   ['email', 'passwd', 'admin', 'name', 'image', 'created_at']
#         # map经过匿名函数把list中的所有元素处理加符号``,处理以后得到一个惰性序列然后通过list输出
#         # escaped_fields为 ['`email`', '`passwd`', '`admin`', '`name`', '`image`', '`created_at`']
#         escaped_fields = list(map(lambda f: '`%s`' % f, fields))
#         print(escaped_fields)
#         attrs['__mappings__'] = mappings # 保存属性和列的映射关系
#         # 保存表名
#         attrs['__table__'] = tableName
#         # 主键属性名
#         attrs['__primary_key__'] = primaryKey 
#         # 除主键外的属性名
#         attrs['__fields__'] = fields 
#         # 构造默认的SELECT, INSERT, UPDATE和DELETE语句:
#         attrs['__select__'] = 'select `%s`, %s from `%s`' % (primaryKey, ', '.join(escaped_fields), tableName)
#         attrs['__insert__'] = 'insert into `%s` (%s, `%s`) values (%s)' % (tableName, ', '.join(escaped_fields), primaryKey, create_args_string(len(escaped_fields) + 1))
#         attrs['__update__'] = 'update `%s` set %s where `%s`=?' % (tableName, ', '.join(map(lambda f: '`%s`=?' % (mappings.get(f).name or f), fields)), primaryKey)
#         attrs['__delete__'] = 'delete from `%s` where `%s`=?' % (tableName, primaryKey)
#         print("修改后attrs:%s" % attrs)
#         return type.__new__(cls, name, bases, attrs)

# class Model(dict, metaclass=ModelMetaclass):
#     pass
# # 定义Field类，作为数据库字段类型的父类
# class Field(object):
#     # 初始化方法定义了4个属性，分别为name字段名(id),column_type字段属性(bigint),primary_key是否为主键(True or False),default默认值
#     def __init__(self, name, column_type, primary_key, default):
#         self.name = name
#         self.column_type = column_type
#         self.primary_key = primary_key
#         self.default = default

#     # 返回实例对象的时候好看一点默认返回为 <__main__.StringField object at 0x0000025CC313EF08>
#     # 定义了__str__返回为 <StringField:email>
#     # 可以省略使用默认也可以
#     def __str__(self):
#         return '<%s, %s:%s>' % (self.__class__.__name__, self.column_type, self.name)

# # 定义字符串字段类,继承父类Field的初始化方法，name字段名属性默认为None，到时使用类User创建出来的实例的key就是字段名
# # 例如创建了一个User实例，该实例是一个字典,它包含的key有id email name等就是数据库表的字段名
# class StringField(Field):

#     def __init__(self, name=None, primary_key=False, default=None, ddl='varchar(100)'):
#         super().__init__(name, ddl, primary_key, default)

# # 定义布尔类型字段类，通过name属性为None
# # 字段类型默认为boolean，默认为非主键，默认值为False
# class BooleanField(Field):

#     def __init__(self, name=None, default=False):
#         super().__init__(name, 'boolean', False, default)

# # 定义整数类型字段类
# class IntegerField(Field):

#     def __init__(self, name=None, primary_key=False, default=0):
#         super().__init__(name, 'bigint', primary_key, default)

# # 定义浮点类型字段类，字段类型real相当于float
# class FloatField(Field):

#     def __init__(self, name=None, primary_key=False, default=0.0):
#         super().__init__(name, 'real', primary_key, default)

# # 定义文本类型字段类
# class TextField(Field):

#     def __init__(self, name=None, default=None):
#         super().__init__(name, 'text', False, default)

# # 定义函数，用于随机生成一个字符串作为主键id的值
# # 加括号运行函数返回一个字符串字符串去前15位为时间戳乘以1000，然后不足15位使用0补齐
# # 后面为使用uuid.uuid4().hex随机生成的一段字符串
# # 最后补000
# def next_id():
#     return '%015d%s000' % (int(time.time() * 1000), uuid.uuid4().hex)

# # 定义用户User类
# class User(Model):
#     # 自定义表名，如果不自定义可以使用类名作为表名
#     # 在使用metaclass重新定义类时，通过方法__new__内部的参数name可以获取到类名
#     __table__ = 'users'
#     # 定义字段名,id作为主键
#     id = StringField(primary_key=True, default=next_id, ddl='varchar(50)')
#     email = StringField(ddl='varchar(50)')
#     passwd = StringField(ddl='varchar(50)')
#     admin = BooleanField()
#     name = StringField(ddl='varchar(50)')
#     image = StringField(ddl='varchar(500)')
#     created_at = FloatField(default=time.time)

# u = User(name='Test', email='test@qq.com', passwd='1234567890', image='about:blank')

# # 拆分解析类User的使用metaclass的修改过程 start
# #$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

# ############################################
# # 拆分解析往类Model添加方法save update remove start
# import time,uuid
# # 根据传递的整数生成一个字符串?,?,?...用于占位符
# def create_args_string(num):
#     L = []
#     for n in range(num):
#         L.append('?')
#     return ', '.join(L)

# class ModelMetaclass(type):

#     def __new__(cls, name, bases, attrs):
#         # 排除Model类本身,即针对Model不做任何修改原样返回:
#         if name=='Model':
#             return type.__new__(cls, name, bases, attrs)
#         # 获取table名称:
#         # 从字典attrs中获取属性__table__,如果在类中没有定义这个属性则返回None
#         # 如果在属性中没有定义但是我们可以从参数name中获取到就是类名
#         # 例如我们对类User进行重新创建,在类User中已经定义了属性 __table__ = 'users'
#         # 所以我们优先得到的表名就是'users',假如没有定义则就是类名'User'
#         tableName = attrs.get('__table__', None) or name
#         # 输出日志
#         logging.info('found model: %s (table: %s)' % (name, tableName))
#         # 为了便于观察我们打印对类修改前的attrs字典
#         print("修改前attrs:%s" % attrs)
#         # 获取所有的Field和主键名:
#         # 定义一个空字典用于存储需要定义的类中除了默认属性以为定义的属性
#         # 例如本次我们针对类User则使用mappings字典存储'id','email','passwd','admin','name','image','create_at'的属性
#         # 它们对应的属性值是一个实例化以后的实例，例如id对应的属性值是通过类StringField实例化后的实例
#         mappings = dict()
#         # fields表用于存储普通的字段名称，即除了主键以外的其他字段
#         # 例如针对User类则在fields存储字段'email','passwd','admin','name','image','create_at'的名称
#         fields = []
#         # primaryKey用于存储主键字段名
#         # 例如针对类User的主键名为'id'
#         primaryKey = None
#         # 以key,value的方式遍历attrs字典
#         for k, v in attrs.items():
#             # 如果对应的value是Field的子集则把对应的key value追加至字典mappings
#             if isinstance(v, Field):
#                 logging.info('  found mapping: %s ==> %s' % (k, v))
#                 mappings[k] = v
#                 # 然后通过实例的属性primary_key去找主键，如果找到了主键则赋值给primaryKey
#                 # 如果不是主键的字段则追加至fields这个list
#                 # 一个表只能有一个主键，如果有多个主键则抛出RuntimeError错误，错误提示为重复的主键
#                 if v.primary_key:
#                     # 找到主键:
#                     if primaryKey:
#                         raise RuntimeError('Duplicate primary key for field: %s' % k)
#                     primaryKey = k
#                 else:
#                     fields.append(k)
#         # 如果表内没有定义主键则抛出错误主键没有发现
#         if not primaryKey:
#             raise RuntimeError('Primary key not found.')
#         # 已经把对应的key value存储至字典以后，从原attrs中属性中删除对应的key value
#         # 如果不删除则类属性和实例属性会冲突
#         # 例如类User需要从原attrs中删除key值为 'id','email','passwd','admin','name','image','create_at'的元素
#         for k in mappings.keys():
#             attrs.pop(k)
#         # 把存储除主键之外的字典list元素加一个``
#         # 例如原fields为   ['email', 'passwd', 'admin', 'name', 'image', 'created_at']
#         # map经过匿名函数把list中的所有元素处理加符号``,处理以后得到一个惰性序列然后通过list输出
#         # escaped_fields为 ['`email`', '`passwd`', '`admin`', '`name`', '`image`', '`created_at`']
#         escaped_fields = list(map(lambda f: '`%s`' % f, fields))
#         print(escaped_fields)
#         attrs['__mappings__'] = mappings # 保存属性和列的映射关系
#         # 保存表名
#         attrs['__table__'] = tableName
#         # 主键属性名
#         attrs['__primary_key__'] = primaryKey 
#         # 除主键外的属性名
#         attrs['__fields__'] = fields 
#         # 构造默认的SELECT, INSERT, UPDATE和DELETE语句:
#         attrs['__select__'] = 'select `%s`, %s from `%s`' % (primaryKey, ', '.join(escaped_fields), tableName)
#         attrs['__insert__'] = 'insert into `%s` (%s, `%s`) values (%s)' % (tableName, ', '.join(escaped_fields), primaryKey, create_args_string(len(escaped_fields) + 1))
#         attrs['__update__'] = 'update `%s` set %s where `%s`=?' % (tableName, ', '.join(map(lambda f: '`%s`=?' % (mappings.get(f).name or f), fields)), primaryKey)
#         attrs['__delete__'] = 'delete from `%s` where `%s`=?' % (tableName, primaryKey)
#         print("修改后attrs:%s" % attrs)
#         return type.__new__(cls, name, bases, attrs)

# class Model(dict, metaclass=ModelMetaclass):
#     @classmethod
#     def find(cls, pk):
#         ## find object by primary key
#         sql = '%s where `%s`=?' % (cls.__select__, cls.__primary_key__)
#         print(sql)
#         rs = select(sql, [pk], 1)     
#         if len(rs) == 0:
#             return None
#         # print(type(cls(**rs[0])),type(rs[0]))
#         # 注意rs[0]是一个字典,使用cls(**rs[0])相当于把这个字典传递给类创建一个实例
#         # 虽然打印rs[0]和cls(**rs[0])看起来是一样的但是类型不一样，不能返回字典因为返回字典就无法调用类的方法
#         return cls(**rs[0])
        

#     # 使字典可以以属性方式取值
#     def __getattr__(self, key):
#         try:
#             return self[key]
#         except KeyError:
#             raise AttributeError(r"'Model' object has no attribute '%s'" % key)
#     def __setattr__(self, key, value):
#         self[key] = value

#     # 定义方法通过key取值
#     def getValue(self, key):
#         return getattr(self, key, None)

#     def getValueOrDefault(self, key):
#         # 首先通过key从字典取值
#         value = getattr(self, key, None)
#         # 如果没有取得值,则从属性__mappings__中去获取
#         if value is None:
#             field = self.__mappings__[key]
#             # 如果对应的实例的default值不为空，则判断如果是可执行对象则加括号执行，取得执行后的值
#             # 否则取默认值
#             if field.default is not None:
#                 value = field.default() if callable(field.default) else field.default
#                 logging.debug('using default value for %s: %s' % (key, str(value)))
#                 setattr(self, key, value)
#         return value
       
#     def save(self):
#         args = list(map(self.getValueOrDefault, self.__fields__))
#         args.append(self.getValueOrDefault(self.__primary_key__))
#         sql = self.__insert__
#         print(sql,args)
#         rows = execute(sql,args)
#         print(rows)
#         if rows != 1:
#             logging.warning('failed to insert record: affected rows: %s' % rows)

#     def update(self):
#         args = list(map(self.getValue, self.__fields__))
#         args.append(self.getValue(self.__primary_key__))
#         rows = execute(self.__update__, args)
#         print(self.__update__,args)
#         if rows != 1:
#             logging.warning('failed to update by primary key: affected rows: %s' % rows)

#     def remove(self):
#         args = [self.getValue(self.__primary_key__)]
#         print(self.__delete__, args)
#         rows = execute(self.__delete__, args)
#         if rows != 1:
#             logging.warning('failed to delete by primary key: affected rows: %s' % rows)
# # 定义Field类，作为数据库字段类型的父类
# class Field(object):
#     # 初始化方法定义了4个属性，分别为name字段名(id),column_type字段属性(bigint),primary_key是否为主键(True or False),default默认值
#     def __init__(self, name, column_type, primary_key, default):
#         self.name = name
#         self.column_type = column_type
#         self.primary_key = primary_key
#         self.default = default

#     # 返回实例对象的时候好看一点默认返回为 <__main__.StringField object at 0x0000025CC313EF08>
#     # 定义了__str__返回为 <StringField:email>
#     # 可以省略使用默认也可以
#     def __str__(self):
#         return '<%s, %s:%s>' % (self.__class__.__name__, self.column_type, self.default)

# # 定义字符串字段类,继承父类Field的初始化方法，name字段名属性默认为None，到时使用类User创建出来的实例的key就是字段名
# # 例如创建了一个User实例，该实例是一个字典,它包含的key有id email name等就是数据库表的字段名
# class StringField(Field):

#     def __init__(self, name=None, primary_key=False, default=None, ddl='varchar(100)'):
#         super().__init__(name, ddl, primary_key, default)

# # 定义布尔类型字段类，通过name属性为None
# # 字段类型默认为boolean，默认为非主键，默认值为False
# class BooleanField(Field):

#     def __init__(self, name=None, default=False):
#         super().__init__(name, 'boolean', False, default)

# # 定义整数类型字段类
# class IntegerField(Field):

#     def __init__(self, name=None, primary_key=False, default=0):
#         super().__init__(name, 'bigint', primary_key, default)

# # 定义浮点类型字段类，字段类型real相当于float
# class FloatField(Field):

#     def __init__(self, name=None, primary_key=False, default=0.0):
#         super().__init__(name, 'real', primary_key, default)

# # 定义文本类型字段类
# class TextField(Field):

#     def __init__(self, name=None, default=None):
#         super().__init__(name, 'text', False, default)

# # 定义函数，用于随机生成一个字符串作为主键id的值
# # 加括号运行函数返回一个字符串字符串去前15位为时间戳乘以1000，然后不足15位使用0补齐
# # 后面为使用uuid.uuid4().hex随机生成的一段字符串
# # 最后补000
# def next_id():
#     return '%015d%s000' % (int(time.time() * 1000), uuid.uuid4().hex)

# # 定义用户User类
# class User(Model):
#     # 自定义表名，如果不自定义可以使用类名作为表名
#     # 在使用metaclass重新定义类时，通过方法__new__内部的参数name可以获取到类名
#     __table__ = 'users'
#     # 定义字段名,id作为主键
#     id = StringField(primary_key=True, default=next_id, ddl='varchar(50)')
#     email = StringField(ddl='varchar(50)')
#     passwd = StringField(ddl='varchar(50)')
#     admin = BooleanField()
#     name = StringField(ddl='varchar(50)')
#     image = StringField(ddl='varchar(500)')
#     created_at = FloatField(default=time.time)
# # 创建数据库连接池和执行insert update select函数 start
# import pymysql
# def create_pool(**kw):
#     host=kw.get('host', 'localhost')
#     port=kw.get('port', 3306)
#     user=kw['user']
#     password=kw['password']
#     db=kw['db']
#     # 创建全部变量，用于存储创建的连接池
#     global conn
#     conn = pymysql.connect(host=host, user=user, password=password, database=db,port=port)
    
# # 字典存储数据库信息
# kw = {'host':'localhost','user':'www-data','password':'www-data','db':'awesome'}

# # 把字典传入创建连接池函数，执行即创建了全局连接池conn，可以在查询，执行等函数调用执行
# create_pool(**kw)

# def select(sql,args,size=None):
#     log(sql,args)
#     cursor =  conn.cursor(pymysql.cursors.DictCursor)
#     cursor.execute(sql.replace('?','%s'),args or ())
#     if size:
#         rs = cursor.fetchmany(size)
#     else:
#         rs = cursor.fetchall()
#     cursor.close
#     logging.info('rows returned: %s' % len(rs))
#     return rs

# def execute(sql,args):
    
#     cursor = conn.cursor(pymysql.cursors.DictCursor)
#     try:
#         cursor.execute(sql.replace('?','%s'),args)
#         rs = cursor.rowcount
#         cursor.close()
#         conn.commit()
#     except:
#         raise
#     return rs
# # 创建数据库连接池和执行insert update select函数 end

# u = User(name='Test', email='test@qq.com', passwd='1234567890', image='about:blank',admin=True)
# # u.save()
# # print(u.getValue('id'))
# # print(select("select * from users",[]))
# rs = User.find('00163764569648657ab15f6c89a483a8f2aa02ece349bba000')
# print(rs)



# # print(rs.getValue('id'))
# #print(rs)
# # rs.email = 'test2@qq.com'
# # rs.remove()
# # rs.email = "test4@qq.com"
# # print(rs)
# # rs.update()

# # 拆分解析往类Model添加方法save update remove  end
# #$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

############################################
# 拆分解析往类Model添加方法select
import time,uuid
# 根据传递的整数生成一个字符串?,?,?...用于占位符
def create_args_string(num):
    L = []
    for n in range(num):
        L.append('?')
    return ', '.join(L)

class ModelMetaclass(type):

    def __new__(cls, name, bases, attrs):
        # 排除Model类本身,即针对Model不做任何修改原样返回:
        if name=='Model':
            return type.__new__(cls, name, bases, attrs)
        # 获取table名称:
        # 从字典attrs中获取属性__table__,如果在类中没有定义这个属性则返回None
        # 如果在属性中没有定义但是我们可以从参数name中获取到就是类名
        # 例如我们对类User进行重新创建,在类User中已经定义了属性 __table__ = 'users'
        # 所以我们优先得到的表名就是'users',假如没有定义则就是类名'User'
        tableName = attrs.get('__table__', None) or name
        # 输出日志
        logging.info('found model: %s (table: %s)' % (name, tableName))
        # 为了便于观察我们打印对类修改前的attrs字典
        print("修改前attrs:%s" % attrs)
        # 获取所有的Field和主键名:
        # 定义一个空字典用于存储需要定义的类中除了默认属性以为定义的属性
        # 例如本次我们针对类User则使用mappings字典存储'id','email','passwd','admin','name','image','create_at'的属性
        # 它们对应的属性值是一个实例化以后的实例，例如id对应的属性值是通过类StringField实例化后的实例
        mappings = dict()
        # fields表用于存储普通的字段名称，即除了主键以外的其他字段
        # 例如针对User类则在fields存储字段'email','passwd','admin','name','image','create_at'的名称
        fields = []
        # primaryKey用于存储主键字段名
        # 例如针对类User的主键名为'id'
        primaryKey = None
        # 以key,value的方式遍历attrs字典
        for k, v in attrs.items():
            # 如果对应的value是Field的子集则把对应的key value追加至字典mappings
            if isinstance(v, Field):
                logging.info('  found mapping: %s ==> %s' % (k, v))
                mappings[k] = v
                # 然后通过实例的属性primary_key去找主键，如果找到了主键则赋值给primaryKey
                # 如果不是主键的字段则追加至fields这个list
                # 一个表只能有一个主键，如果有多个主键则抛出RuntimeError错误，错误提示为重复的主键
                if v.primary_key:
                    # 找到主键:
                    if primaryKey:
                        raise RuntimeError('Duplicate primary key for field: %s' % k)
                    primaryKey = k
                else:
                    fields.append(k)
        # 如果表内没有定义主键则抛出错误主键没有发现
        if not primaryKey:
            raise RuntimeError('Primary key not found.')
        # 已经把对应的key value存储至字典以后，从原attrs中属性中删除对应的key value
        # 如果不删除则类属性和实例属性会冲突
        # 例如类User需要从原attrs中删除key值为 'id','email','passwd','admin','name','image','create_at'的元素
        for k in mappings.keys():
            attrs.pop(k)
        # 把存储除主键之外的字典list元素加一个``
        # 例如原fields为   ['email', 'passwd', 'admin', 'name', 'image', 'created_at']
        # map经过匿名函数把list中的所有元素处理加符号``,处理以后得到一个惰性序列然后通过list输出
        # escaped_fields为 ['`email`', '`passwd`', '`admin`', '`name`', '`image`', '`created_at`']
        escaped_fields = list(map(lambda f: '`%s`' % f, fields))
        print(escaped_fields)
        attrs['__mappings__'] = mappings # 保存属性和列的映射关系
        # 保存表名
        attrs['__table__'] = tableName
        # 主键属性名
        attrs['__primary_key__'] = primaryKey 
        # 除主键外的属性名
        attrs['__fields__'] = fields 
        # 构造默认的SELECT, INSERT, UPDATE和DELETE语句:
        attrs['__select__'] = 'select `%s`, %s from `%s`' % (primaryKey, ', '.join(escaped_fields), tableName)
        attrs['__insert__'] = 'insert into `%s` (%s, `%s`) values (%s)' % (tableName, ', '.join(escaped_fields), primaryKey, create_args_string(len(escaped_fields) + 1))
        attrs['__update__'] = 'update `%s` set %s where `%s`=?' % (tableName, ', '.join(map(lambda f: '`%s`=?' % (mappings.get(f).name or f), fields)), primaryKey)
        # attrs['__update__'] = 'update `%s` set %s where `%s`=?' % (tableName, ', '.join(map(lambda f: '%s=?' % f, escaped_fields)), primaryKey)
        attrs['__delete__'] = 'delete from `%s` where `%s`=?' % (tableName, primaryKey)
        print("修改后attrs:%s" % attrs)
        return type.__new__(cls, name, bases, attrs)

class Model(dict, metaclass=ModelMetaclass):
    @classmethod
    def findAll(cls,where=None,agrs=None,**kw):
        # 原始select语句 'select `id`, `email`, `passwd`, `admin`, `name`, `image`, `created_at` from `users`'
        # 原始是一个str,放在一个list中,即把这个str作为list的一个元素
        sql = [cls.__select__]
        print('SQL原始放入list中后是:%s' %(sql))
        if where:
            sql.append('where')
            sql.append(where)
        print('SQL加入where条件后是:%s' %(sql))
        if agrs is None:
            args = []
        orderBy = kw.get('orderBy',None)
        if orderBy:
            sql.append('order by')
            sql.append(orderBy)
        print('SQL加入order by条件后是:%s' %(sql))
        limit = kw.get('limit',None)
        if limit:
            sql.append('limit')
            if isinstance(limit,int):
                sql.append('?')
                args.append(limit)
            elif isinstance(limit,tuple) and len(limit) == 2:
                sql.append('?,?')
                args.extend(limit)
            else:
                raise ValueError('Invalid limit value: %s' % str(limit))
        print('SQL加入limit条件后是:%s' %(sql))
        print('SQL最后的查询语句是%s,%s' %(' '.join(sql),args))
        rs = select(' '.join(sql),args)
        # 查询结果是一个list其中的元素是字典
        # 使用列表生成器把字典代入类中返回实例
        return [cls(**r) for r in rs]

    @classmethod
    # selectField传递一个字段名称例如id
    # 通过该字段去查询然后把查询到的第一个结果对应的值返回
    # 可以通过查询'count(id)'返回的是整数代表
    # 该表有多少条数据
    def findNumber(cls,selectField,where=None,args=None):
        sql = ['select %s _num_ from `%s`' % (selectField, cls.__table__)]
        if where:
            sql.append('where')
            sql.append(where)
        print('SQL通过findNumber查询语句是:%s,%s' %(' '.join(sql),args))
        rs = select(' '.join(sql),args,1)
        if len(rs) == 0:
            return None
        print('findNumber查询结果是%s' %(rs))
        # 取该结果对应的值返回
        # 例如查询'count(id)'查询结果是[{'_num_': 3}]
        # 返回列表下标为0的字典，然后取字典的属性'_num_'值即本次查询这个表包含多少条数据
        return rs[0]['_num_']

    @classmethod
    def find(cls, pk):
        ## find object by primary key
        sql = '%s where `%s`=?' % (cls.__select__, cls.__primary_key__)
        print(sql)
        rs = select(sql, [pk], 1)     
        if len(rs) == 0:
            return None
        # print(type(cls(**rs[0])),type(rs[0]))
        # 注意rs[0]是一个字典,使用cls(**rs[0])相当于把这个字典传递给类创建一个实例
        # 虽然打印rs[0]和cls(**rs[0])看起来是一样的但是类型不一样，不能返回字典因为返回字典就无法调用类的方法
        return cls(**rs[0])
        

    # 使字典可以以属性方式取值
    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            raise AttributeError(r"'Model' object has no attribute '%s'" % key)
    def __setattr__(self, key, value):
        self[key] = value

    # 定义方法通过key取值
    def getValue(self, key):
        return getattr(self, key, None)

    def getValueOrDefault(self, key):
        # 首先通过key从字典取值
        value = getattr(self, key, None)
        # 如果没有取得值,则从属性__mappings__中去获取
        if value is None:
            field = self.__mappings__[key]
            # 如果对应的实例的default值不为空，则判断如果是可执行对象则加括号执行，取得执行后的值
            # 否则取默认值
            if field.default is not None:
                value = field.default() if callable(field.default) else field.default
                logging.debug('using default value for %s: %s' % (key, str(value)))
                setattr(self, key, value)
        return value
       
    def save(self):
        args = list(map(self.getValueOrDefault, self.__fields__))
        args.append(self.getValueOrDefault(self.__primary_key__))
        sql = self.__insert__
        print(sql,args)
        rows = execute(sql,args)
        print(rows)
        if rows != 1:
            logging.warning('failed to insert record: affected rows: %s' % rows)

    def update(self):
        args = list(map(self.getValue, self.__fields__))
        args.append(self.getValue(self.__primary_key__))
        rows = execute(self.__update__, args)
        print(self.__update__,args)
        if rows != 1:
            logging.warning('failed to update by primary key: affected rows: %s' % rows)

    def remove(self):
        args = [self.getValue(self.__primary_key__)]
        print(self.__delete__, args)
        rows = execute(self.__delete__, args)
        if rows != 1:
            logging.warning('failed to delete by primary key: affected rows: %s' % rows)
# 定义Field类，作为数据库字段类型的父类
class Field(object):
    # 初始化方法定义了4个属性，分别为name字段名(id),column_type字段属性(bigint),primary_key是否为主键(True or False),default默认值
    def __init__(self, name, column_type, primary_key, default):
        self.name = name
        self.column_type = column_type
        self.primary_key = primary_key
        self.default = default

    # 返回实例对象的时候好看一点默认返回为 <__main__.StringField object at 0x0000025CC313EF08>
    # 定义了__str__返回为 <StringField:email>
    # 可以省略使用默认也可以
    def __str__(self):
        return '<%s, %s:%s>' % (self.__class__.__name__, self.column_type, self.default)

# 定义字符串字段类,继承父类Field的初始化方法，name字段名属性默认为None，到时使用类User创建出来的实例的key就是字段名
# 例如创建了一个User实例，该实例是一个字典,它包含的key有id email name等就是数据库表的字段名
class StringField(Field):

    def __init__(self, name=None, primary_key=False, default=None, ddl='varchar(100)'):
        super().__init__(name, ddl, primary_key, default)

# 定义布尔类型字段类，通过name属性为None
# 字段类型默认为boolean，默认为非主键，默认值为False
class BooleanField(Field):

    def __init__(self, name=None, default=False):
        super().__init__(name, 'boolean', False, default)

# 定义整数类型字段类
class IntegerField(Field):

    def __init__(self, name=None, primary_key=False, default=0):
        super().__init__(name, 'bigint', primary_key, default)

# 定义浮点类型字段类，字段类型real相当于float
class FloatField(Field):

    def __init__(self, name=None, primary_key=False, default=0.0):
        super().__init__(name, 'real', primary_key, default)

# 定义文本类型字段类
class TextField(Field):

    def __init__(self, name=None, default=None):
        super().__init__(name, 'text', False, default)

# 定义函数，用于随机生成一个字符串作为主键id的值
# 加括号运行函数返回一个字符串字符串去前15位为时间戳乘以1000，然后不足15位使用0补齐
# 后面为使用uuid.uuid4().hex随机生成的一段字符串
# 最后补000
def next_id():
    return '%015d%s000' % (int(time.time() * 1000), uuid.uuid4().hex)

# 定义用户User类
class User(Model):
    # 自定义表名，如果不自定义可以使用类名作为表名
    # 在使用metaclass重新定义类时，通过方法__new__内部的参数name可以获取到类名
    __table__ = 'users'
    # 定义字段名,id作为主键
    id = StringField(primary_key=True, default=next_id, ddl='varchar(50)')
    email = StringField(ddl='varchar(50)')
    passwd = StringField(ddl='varchar(50)')
    admin = BooleanField()
    name = StringField(ddl='varchar(50)')
    image = StringField(ddl='varchar(500)')
    created_at = FloatField(default=time.time)
# 创建数据库连接池和执行insert update select函数 start
import pymysql
def create_pool(**kw):
    host=kw.get('host', 'localhost')
    port=kw.get('port', 3306)
    user=kw['user']
    password=kw['password']
    db=kw['db']
    # 创建全部变量，用于存储创建的连接池
    global conn
    conn = pymysql.connect(host=host, user=user, password=password, database=db,port=port)
    
# 字典存储数据库信息
kw = {'host':'localhost','user':'www-data','password':'www-data','db':'awesome'}

# 把字典传入创建连接池函数，执行即创建了全局连接池conn，可以在查询，执行等函数调用执行
create_pool(**kw)

def select(sql,args,size=None):
    log(sql,args)
    cursor =  conn.cursor(pymysql.cursors.DictCursor)
    cursor.execute(sql.replace('?','%s'),args or ())
    if size:
        rs = cursor.fetchmany(size)
    else:
        rs = cursor.fetchall()
    cursor.close
    logging.info('rows returned: %s' % len(rs))
    return rs

def execute(sql,args):
    
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    try:
        cursor.execute(sql.replace('?','%s'),args)
        # rowcount方法把影响函数返回
        rs = cursor.rowcount
        cursor.close()
        conn.commit()
    except:
        raise
    return rs
# 创建数据库连接池和执行insert update select函数 end

u = User(name='Test', email='test10@qq.com', passwd='1234567890', image='about:blank',admin=False)
u.save()
# u.save()
# print(u.getValue('id'))
# print(select("select * from users",[]))
# rs = User.findAll()
# print(rs)
# rs = User.find('0')
# print(rs)
# rs['email'] = 'asdg11111asdah@qq.com'
# rs.update()
# rs = User.findAll(where="id='00163764569648657ab15f6c89a483a8f2aa02ece349bba000'",orderBy='id',limit=1)
# print(rs)
# rs = User.findNumber(selectField='created_at')
# print(rs)
# rs = User.find('1')
# print(rs)
# 拆分解析往类Model添加方法select  end
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
# rs = User.findNumber('count(id)')
# rs = User.findAll(orderBy='created_at desc', limit=(0, 8))
# print(rs)
rs = User.findNumber('count(id)')
print(rs)