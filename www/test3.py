# import handlers
# module_name = 'handlers'
# n = module_name.rfind('.')
# print(n)
# mod = __import__(module_name, globals(), locals())
# print(dir(mod))

# for attr in dir(mod):
#     if attr.startswith('_'):
#         continue
#     fn = getattr(mod, attr)
#     if callable(fn):
#         method = getattr(fn, '__method__', None)
#         path = getattr(fn, '__route__', None)
#         print(method,path)
#         # if method and path:
# #         #         add_route(app, fn)
# # import os
# # print(os.path.abspath(__file__))
# # print(os.path.dirname(os.path.abspath(__file__)))
# # print(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates'))


# from inspect import signature
# def foo(a, *, b:int, **kwargs):
#     pass

# sig = signature(foo)
# print(sig)
# print(str(sig))
# #'(a, *, b:int, **kwargs)'

# print(str(sig.parameters['b']))

# def foo1(atr,*,arr):
#     print(atr)
#     print(arr)


# foo1(atr='123',arr=False)
# def f1(a, *, b, c):
#     return a + b + c

# print(f1(2,b=3,c=4))

# params = signature(foo).parameters
# print(params)
# for name,param in params.items():
#     print(name,param)
#     print(param.kind,param.default)

# def foo(a,*b,c,**args):
#     pass
# params = signature(foo).parameters
# print(params)
# for name,param in params.items():
#     print(name,param)
#     print(param.kind,param.default)

# class Person(object):
#   def __init__(self, name, gender):
#     self.name = name
#     self.gender = gender
 
#   def __call__(self, friend):
#     print('My name is %s...' % self.name)
#     print('My friend is %s...' % friend)

# p=Person('Michael','male')
# p('133')

# import handlers
# module_name = 'handlers'
# n = module_name.rfind('.')
# print(n)
# mod = __import__(module_name, globals(), locals())
# print(mod)
# print(dir(mod))
# for attr in dir(mod):
#     if attr.startswith('_'):
#         continue
#     fn = getattr(mod, attr)
#     if callable(fn):
#         method = getattr(fn, '__method__', None)
#         path = getattr(fn, '__route__', None)
#         print(method,path)
        # if method and path:
        #     add_route(app, fn)
   
# import aiohttp
# print(123)

# class Student(object):
#     def __init__(self,name):
#         self.name = name

#     def __call__(self):
#         print('My name is %s' %(self.name))

# s = Student('Michale')
# s()

# class Chain(object):
#     def __init__(self, path=''):
#        self.__path = path
 
#     def __getattr__(self, path):
#        return Chain('%s/%s' % (self.__path, path))
 
#     def __call__(self, path):
#        return Chain('%s/%s' % (self.__path, path))
 
#     def __str__(self):
#        return self.__path
 
#    # __repr__ = __str__
 
# print(Chain().users('michael').repos)
#import handlers
# mod = __import__('handlers')
# # 获得一个模块对象
# print(mod)
# # 打印模块对应的方法和属性
# print(dir(mod,locals(),globals()))
# ['User', '__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'asyncio', 'get', 'index', 'logging']

def get_page_index(page_str):
    p = 1
    try:
        p = int(page_str)
    except ValueError as e:
        pass
    if p < 1:
        p = 1
    return p

print(get_page_index('abc'))

import time
import hashlib
_COOKIE_KEY = 'Aewsome'
def user2cookie(user, max_age):
    # build cookie string by: id-expires-sha1
    expires = str(int(time.time() + max_age))
    s = '%s-%s-%s-%s' % (user.id, user.passwd, expires, _COOKIE_KEY)
    L = [user.id, expires, hashlib.sha1(s.encode('utf-8')).hexdigest()]
    return '-'.join(L)

class User(dict):
    def __getattr__(self,key):
        return self[key]

user = User(id='001637895583499da19dda790484f7e8938266ecc126496000',passwd='1234567890')
print(user2cookie(user,10))