import logging; logging.basicConfig(level=logging.INFO)
from datetime import datetime
from aiohttp import web
import os
from jinja2 import Environment, FileSystemLoader
def init_jinja2(app, **kw):
    logging.info('init jinja2...')
    options = dict(
        autoescape = kw.get('autoescape', True),
        block_start_string = kw.get('block_start_string', '{%'),
        block_end_string = kw.get('block_end_string', '%}'),
        variable_start_string = kw.get('variable_start_string', '{{'),
        variable_end_string = kw.get('variable_end_string', '}}'),
        auto_reload = kw.get('auto_reload', True)
    )
    print(options)
    path = kw.get('path', None)
    if path is None:
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
    logging.info('set jinja2 template path: %s' % path)
   
   
    env = Environment(loader=FileSystemLoader(path), **options)
    #print(env.__dict__)
    print(kw)
    filters = kw.get('filters', None)
    if filters is not None:
        for name, f in filters.items():
            env.filters[name] = f 

    app['__templating__'] = env


## 时间转换
def datetime_filter(t):
    delta = int(time.time() - t)
    if delta < 60:
        return u'1分钟前'
    if delta < 3600:
        return u'%s分钟前' % (delta // 60)
    if delta < 86400:
        return u'%s小时前' % (delta // 3600)
    if delta < 604800:
        return u'%s天前' % (delta // 86400)
    dt = datetime.fromtimestamp(t)
    return u'%s年%s月%s日' % (dt.year, dt.month, dt.day)


app = web.Application()
init_jinja2(app, filters=dict(datetime=datetime_filter))
# print('默认的env.__dict__:%s' % (Environment().__dict__) )
# print('初始化以后的__dict__:%s' %(app['__templating__'].__dict__))

dict1 = Environment().__dict__
dict2 = app['__templating__'].__dict__

for k,v in dict1.items():
    print(k,v)

for k,v in dict2.items():
    print(k,v)

print(app['__templating__'].get_template(template).render(**r).encode('utf-8'))