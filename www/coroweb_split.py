# # get
# import functools
# def get(path):
#     ## Define decorator @get('/path')
#     def decorator(func):
        
#         @functools.wraps(func)
#         def wrapper(*args, **kw):
#             return func(*args, **kw)
#         print(dir(wrapper))
#         wrapper.__method__ = 'GET'
#         wrapper.__route__ = path
#         print(dir(wrapper))
#         return wrapper
#     return decorator

# @get('/')
# def index(request):
#     pass

# import inspect

# params = inspect.signature(index).parameters
# print(params)
# for k,v in params.items():
#     print(v.kind)
# print(index.__route__)
from inspect import signature
def foo(a, *, b:int, **kwargs):
    pass

sig = signature(foo)


print(str(sig))
#'(a, *, b:int, **kwargs)'

print(str(sig.parameters['b']))
# 'b:int'

print(sig.parameters['b'].annotation)
# <class 'int'>
import inspect
print(inspect.isfunction(foo))